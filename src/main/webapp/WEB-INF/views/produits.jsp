<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>

<title>catalogue</title>
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/resources/css/style.css">
</head>
<body>
    <div id="formProduit">
        <f:form modelAttribute="prod" method="post" action="saveProduit" >
            <table>
                 <tr>
                    <td>Reference:</td>
                    <td><f:input path="reference"/></td>
                    <td><f:errors path="reference" cssClass="errors"/></td>
                 </tr>
                 <tr>
                    <td>DESIGNATION:</td>
                    <td><f:input path="designation"/></td>
                    <td><f:errors path="designation" cssClass="errors"/></td>
                 </tr>
                  <tr>
                    <td>PRICE:</td>
                    <td><f:input path="prix"/></td>
                    <td><f:errors path="prix" cssClass="errors"/></td>
                 </tr>
                  <tr>
                    <td>QUANTITE:</td>
                    <td><f:input path="qte"/></td>
                    <td><f:errors path="qte" cssClass="errors"/></td>
                 </tr>
                  <tr>
                    <td><input type="submit" value="save"></td>
                  </tr>
                 
            </table>
        </f:form>
    </div>
	<div id="listProduits">
		<table class="table1">
			<tr>
				<th>ref</th>
				<th>designation</th>
				<th>price</th>
				<th>quantity</th>
			</tr>
			<c:forEach items="${prods}" var="p">
				<tr>
					<td>${p.reference}</td>
					<td>${p.designation}</td>
					<td>${p.prix}</td>
					<td>${p.qte}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>