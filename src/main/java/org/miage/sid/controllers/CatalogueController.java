package org.miage.sid.controllers;

import javax.validation.Valid;

import org.miage.sid.dao.Produit;
import org.miage.sid.metier.ICatalogueMetier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller  // composant spring
public class CatalogueController {
	@Autowired  // ne fonctionne que sur composant  spring
	private ICatalogueMetier metier ; // couplage faible liee uniq a interface

	
	@RequestMapping("/index")
	public String index(Model model) {
		model.addAttribute("prod", new Produit() );
		model.addAttribute("prods", metier.getAllProduits());
		return "produits";
	}
	@RequestMapping("/saveProduit")
	public String save(@Valid Produit p ,BindingResult  bindingResult ,Model model) {
		if(bindingResult.hasErrors()) {
			model.addAttribute("prods", metier.getAllProduits());
			return "produits";
		}
		metier.addProduit(p);
		model.addAttribute("prod", new Produit() );
		model.addAttribute("prods", metier.getAllProduits());
		return "produits";
	}
}
