package org.miage.sid.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class CatalogueDaoImp implements ICatalogueDao {

	Map<String, Produit> produits = new HashMap<String, Produit>();
	private static final Logger logger = LoggerFactory.getLogger(CatalogueDaoImp.class);
	@Override
	public void addProduit(Produit p) {
		
		  produits.put(p.getReference()	, p);
	}

	@Override
	public List<Produit> getAllProduits() {
		Collection<Produit> prods = produits.values();
		
		return new ArrayList<Produit>(prods);
	}

	@Override
	public List<Produit> getProduitParMC(String mc) {
		List<Produit> prods = new ArrayList<Produit>();
		
		for(Produit p:produits.values())
			if(p.getDesignation().indexOf(mc)>=0)
				prods.add(p);
		
		
		return prods;
	}

	@Override
	public Produit getProduit(String ref) {
		
		return produits.get(ref);
	}

	@Override
	public void deleteProduit(String ref) {
		produits.remove(ref);
		
	}

	@Override
	public void updateProduit(Produit p) {
	   produits.put(p.getReference(), p);
		
	}
	public void init() {
		logger.info("instantiation du catalogue");
		addProduit(new Produit("1", "doliprane", 15.8, 7));
		addProduit(new Produit("2", "amoxil 1g/12", 55, 2));
		addProduit(new Produit("3", "erlus sirop", 31.8, 1));
	}

}
