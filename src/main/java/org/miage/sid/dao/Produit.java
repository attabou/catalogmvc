package org.miage.sid.dao;

import java.io.Serializable;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

public class Produit implements Serializable{
	@NotEmpty
	private String reference;
	@NotEmpty
	private String designation;
	@DecimalMin(value = "0.5")
	private double prix;
	private int qte;
	
	public Produit() {
		
	}
	
	public Produit(String reference, String designation, double prix, int qte) {
		super();
		this.reference = reference;
		this.designation = designation;
		this.prix = prix;
		this.qte = qte;
	}

	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public double getPrix() {
		return prix;
	}
	public void setPrix(double prix) {
		this.prix = prix;
	}
	public int getQte() {
		return qte;
	}
	public void setQte(int qte) {
		this.qte = qte;
	}
	
	

}
